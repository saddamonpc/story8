from django.test import TestCase, Client
from django.urls import resolve
from django.apps import apps
from .apps import *
from .views import *
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
import unittest
import time

# Unit Testing
class Story8Test (TestCase):
    def testApps(self):
        self.assertEqual(TugasConfig.name, 'tugas')
        self.assertEqual(apps.get_app_config('tugas').name, 'tugas')

    def testHomepageURL(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def testHomepageUsingTemplate(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'homepage.html')

    def testHomepageUsingHomepageFunction(self):
        found = resolve('/')
        self.assertEqual(found.func, homepage)

# Functional Testing
class Story8FunctionalTest(unittest.TestCase):
    def setUp(self):
        chromeOptions = Options()
        chromeOptions.add_argument('--no-sandbox')
        chromeOptions.add_argument("--headless")
        self.browser = webdriver.Chrome(options=chromeOptions)

    def tearDown(self):
        self.browser.quit()

    def testForHomeTitle(self):
        self.browser.get('http://127.0.0.1:8000/')
        self.assertIn("ppwstory8", self.browser.title)

    def testIfAccordionsOpenAndClose(self):
        self.browser.get('http://127.0.0.1:8000/')
        button = self.browser.find_element_by_id('currentActivities')
        button.click()
        time.sleep(5)
        self.assertEqual(self.browser.find_element_by_id('currentActivities').get_attribute("aria-expanded"), "true")
        button.click()
        time.sleep(5)
        self.assertEqual(self.browser.find_element_by_id('currentActivities').get_attribute("aria-expanded"), "false")
        button = self.browser.find_element_by_id('organizationExperience')
        button.click()
        time.sleep(5)
        self.assertEqual(self.browser.find_element_by_id('organizationExperience').get_attribute("aria-expanded"), "true")
        button.click()
        time.sleep(5)
        self.assertEqual(self.browser.find_element_by_id('organizationExperience').get_attribute("aria-expanded"), "false")
        button = self.browser.find_element_by_id('committeeExperience')
        button.click()
        time.sleep(5)
        self.assertEqual(self.browser.find_element_by_id('committeeExperience').get_attribute("aria-expanded"), "true")
        button.click()
        time.sleep(5)
        self.assertEqual(self.browser.find_element_by_id('committeeExperience').get_attribute("aria-expanded"), "false")
        button = self.browser.find_element_by_id('achievements')
        button.click()
        time.sleep(5)
        self.assertEqual(self.browser.find_element_by_id('achievements').get_attribute("aria-expanded"), "true")
        button.click()
        time.sleep(5)
        self.assertEqual(self.browser.find_element_by_id('achievements').get_attribute("aria-expanded"), "false")

    def testIfAnAccordionMovesUpAndDown(self):
        self.browser.get('http://127.0.0.1:8000/')
        time.sleep(5)
        self.browser.find_element_by_id('upForOrganizationImage').click()
        card = self.browser.find_element_by_xpath("//div[@id='accordionID']/div[@class='card'][2]")
        self.assertIn("second", card.get_attribute("id"))
        time.sleep(5)
        self.browser.find_element_by_id('downForOrganizationImage').click()
        card = self.browser.find_element_by_xpath("//div[@id='accordionID']/div[@class='card'][2]")
        self.assertIn("third", card.get_attribute("id"))








    
